'use strict';
angular.module('wud.services', [])
    /**USERS SERVICE**/
    .service('UsersModel', function($rootScope, $http) {
        var usersService = this;
        usersService.GET_USERS_URL = $rootScope.BASE_URL + '/users';
        usersService.NEW_USER_URL = $rootScope.BASE_URL + '/user';
        usersService.getAllUsers = function() {
            var promise = new Promise(function(resolve, reject) {
                $http({
                    method: 'GET',
                    url: usersService.GET_USERS_URL
                }).then(function successCallback(response) {
                    resolve(response);
                }, function errorCallback(response) {
                    reject(response);
                });
            });
            return promise;
        };
        usersService.newUser = function(user) {
            var promise = new Promise(function(resolve, reject) {
                $http.post(usersService.NEW_USER_URL, user).then(function successCallback(response) {
                    resolve(response);
                }, function errorCallback(response) {
                    reject(response);
                });
            });
            return promise;
        };
    });