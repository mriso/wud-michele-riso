(function() {
    'use strict';
    angular.module('wud.techtest').directive('wudNavbar', wudNavbar).directive('stateNavbar', stateNavbar);
    /** @ngInject */
    function wudNavbar() {
        return {
            restrict: 'E',
            templateUrl: 'app/components/navbar/navbar.html'
        };
    }
    /** @ngInject */
    function stateNavbar($state) {
        return {
            restrict: 'EA',
            template: ' <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">' + '<ul class="nav navbar-nav">' + '<li ng-repeat="state in states" ng-if="state.name && state.name!=\'main\'"><a ui-sref="{{state.name}}">{{state.shownName}}</a></li>' + '</ul></div></div>',
            link: function(scope) {
                scope.states = $state.get();
              
            }
        };
    }
})();