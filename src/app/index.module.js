(function() {
    'use strict';
    angular.module('wud.techtest', ['ui.router', 'ui.bootstrap', 'wud.services'])
        /**RUN**/
        .run(function($rootScope) {
            $rootScope.BASE_URL = 'http://localhost:8000';
        });
})();