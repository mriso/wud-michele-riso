(function() {
    'use strict';
    angular.module('wud.techtest').controller('NewUserController', NewUserController);
    /** @ngInject */
    function NewUserController(UsersModel, $log, $timeout) {
        var vm = this;
        vm.confirmationPending = true;
        vm.createUser = function() {
            if (vm.name && vm.surname && vm.email) {
                UsersModel.newUser({
                    firstname: vm.name,
                    lastname: vm.surname,
                    email: vm.email
                }).then(function(response) {
                    $log.log(response);
                    $timeout(function() {
                        vm.confirmationError = false;
                        vm.confirmationOK= true;
                        vm.confirmationPending = false;
                    }, 0);
                }).catch(function(error) {
                    $log.error(error);
                });
            } else {
                vm.confirmationError = true;
                vm.confirmationOK = false;
                vm.confirmationPending = false;
            }
        };
    }
})();