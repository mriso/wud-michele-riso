(function() {
    'use strict';
    angular.module('wud.techtest').controller('UsersController', UsersController);
    /** @ngInject */
    function UsersController(UsersModel, $log, $timeout) {
        var vm = this;
        UsersModel.getAllUsers().then(function(response) {
            $log.log(response);
            $timeout(function() {
                vm.users = response.data;
            }, 0);
        }).catch(function(error) {
            $log.error(error);
        });
    }
})();